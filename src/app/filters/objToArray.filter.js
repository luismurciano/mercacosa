angular.module('app').filter('toArray', function() {
    return function(obj) {
      var array = [];
      angular.forEach(obj, function (item) {
        array.push(item)
      })
      return array;
    };
  })
