angular
  .module('app')
  .component('app', {
    templateUrl: 'app/hello.html',
    controller: ['$http', '$log', function ($http, $log) {
      var vm = this;
      vm.updateItem = updateItem;
      vm.calculaTotal = calculaTotal

      vm.title = 'Mercacosa';
      vm.productos = {}

      function calculaTotal(listaProductos) {
        var total = 0;
        angular.forEach(listaProductos ,function (item) {
          total += item.precio * (item.cantidad || 0);
        })
        return total
      }

      function updateItem(itemId, cantidad) {
        vm.productos[itemId].cantidad = cantidad;
        vm.total = calculaTotal(vm.productos)
      }

      $http.get('/mocks/productos.json')
      .then(function (response) {
        vm.productos = response.data
        $log.log(response)
      })
      .catch(function (error) {
        $log.error(error)
      })
    }]
  });
