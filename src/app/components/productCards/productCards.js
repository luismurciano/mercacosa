angular
.module('app')
.component('productCards', {
  templateUrl: 'app/components/productCards/productCards.html',
  bindings: {
    listaProductos: '<products', // one-way binding
    onUpdateItem: '&'
  },
  controller: ['$log', '$mdDialog', function ($log, $mdDialog) {
    var vm = this;
      vm.$onInit = function () {
      $log.log('Hello there!', vm.products);
    }

  }]
})
