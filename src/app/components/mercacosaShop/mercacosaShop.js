angular.module('app').component('mercacosaShop', {
  transclude: {
    card: 'productCards',
    table: 'productTable',
    resumen: 'mdCard'

  },
  templateUrl: 'app/components/mercacosaShop/mercacosaShop.html'
})
