angular
  .module('app')
  .component('customHeader', {
    templateUrl: 'app/components/customHeader/customHeader.html',
    bindings: {
      title: '@', // string binding
      subTitle: '@',
      info: '<', // one-way binding
      callback:'&', // function binding
      lame: '=' // two-way binding
    },
    controller: ['$log', '$mdDialog', function ($log, $mdDialog) {
      var vm = this;
      vm.showMeTheModal = showMeTheModal;
      vm.$onInit = function () {
        $log.log('Hello there!', vm.info);
      }

      function showMeTheModal() {
        var alertConfig = $mdDialog.alert({
          title: 'Attention',
          textContent: 'This is an example of how easy dialogs can be!',
          ok: 'Close'
        });
        $mdDialog.show(alertConfig);
      }
    }]
  })
