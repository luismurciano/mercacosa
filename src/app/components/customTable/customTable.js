angular
  .module("app")
  .component("productTable",{
    controllerAs:'tabla',
    templateUrl:'app/components/customTable/customTable.html',
    bindings:{
      title:'@',
      list:'<products',
      onUpdateItem: '&'
    },
    controller: [function(){
    var vm = this;

    vm.$onInit = function () {
      vm.calculoTotal = calculoTotal;

    }
    vm.log = console.log;
    function calculoTotal(list) {

      var resultadoSuma = 0;
      angular.forEach(list, function(resultado){
        if(resultado != undefined){
          resultadoSuma += resultado.precio*resultado.cantidad;
        }

      })
      return resultadoSuma;
    }
    }]
  })
